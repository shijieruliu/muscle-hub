$(function() {
    //检查电话号码正确性: 11位数字
    function checkPhone(phoneNum) {
        let flag = true;
        // 验证长度和每一位
        if (phoneNum.length != 11) {
            flag = false;
        } else {
            if (isNaN(phoneNum)) {
                flag = false;
            }
        }
        return flag;
    }

    //检查密码正确性：6到16位
    function checkPsd(psd) {
        if (psd.length > 16 || psd.length < 6)
            return false;
        return true;
    }


    //密码登录和短信登录的切换
    $(".psd_way, .msg_way").on("click", function() {
        $(this).addClass("current_way");
        $(this).siblings().removeClass("current_way");

        if ($(".psd_way").hasClass("current_way")) {
            $(".psd_login").show();
            $(".msg_login").hide();
        } else {
            $(".msg_login").show();
            $(".psd_login").hide();
        }
    })

    //密码登录选项
    //验证Hub账号输入格式的合法性，长度：5、6、8对应admin, trainer, member三种身份 
    //Hub账号可以是英文字母或者数字
    //正确的账号显示后，有提示信息
    $("#account_ipt").blur(function() {
        let account = $(this).val();
        if (account !== "") {
            let flag = true;
            if (account.length === 5 || account.length === 6 || account.length === 8) {
                for (let i = 0; i < account.length; i++) {
                    if (!(!isNaN(account[i]) || account[i] >= 'a' && account[i] <= 'z' || account[i] >= 'A' && account[i] <= 'Z'))
                        flag = false;
                }
            } else flag = false;

            // 根据flag判断结果
            if (flag) {
                let content = "Hub账号格式正确,您的身份是：";
                switch (account.length) {
                    case 5:
                        content += "管理员";
                        break;
                    case 6:
                        content += "教练";
                        break;
                    case 8:
                        content += "成员";
                        break;
                }
                $(this).siblings(".state").hide();
                $("#correct_msg").text(content);
                $(this).siblings(".state.correct_state").show();
            } else {
                //账号长度错误，显示错误信息
                $(this).siblings(".state").hide();
                $(this).siblings(".state.wrong_state").show();
            }


        } else {
            $(this).siblings(".state").hide();
        }
    })

    //检查密码（仅仅检查位数6-16）
    $("#psd_ipt").blur(function() {
        if ($(this).val() !== "") {
            if (!checkPsd($(this).val())) {
                $(this).siblings(".state.correct_state").hide();
                $(this).siblings(".state.wrong_state").show();
            } else {
                $(this).siblings(".state.correct_state").show();
                $(this).siblings(".state.wrong_state").hide();
            }
        } else {
            $(this).siblings(".state").hide();
        }
    })

    //验证手机号输入格式的合法性（这里为了简化，不考虑外国手机号，认为手机号合法长度为11位）
    $("#phone_ipt").blur(function() {
        var phoneNum = $(this).val();
        if (phoneNum !== "") {
            let flag = checkPhone(phoneNum);

            if (flag) {
                $(this).siblings(".state").hide();
                $(this).siblings(".state.correct_state").show();
            } else {
                $(this).siblings(".state").hide();
                $(this).siblings(".state.wrong_state").show();
            }

        } else {
            $(this).siblings(".state").hide();
        }
    })


    // 点击“登录”按钮时的表单验证功能
    $("#login_btn").click(function() {
        let flag = true; //指示表单是否满足提交条件：所有表单项填满 + 密码正确、手机号正确

        //1. 密码登录方式
        if ($(".psd_login").css("display") === "block") {
            //判断表单的所有项是否都填满了
            if ($("#psd_ipt").val() === "" || $("#psd_ipt").val() === undefined || $("#account_ipt").val() === "" || $("#account_ipt").val() === undefined)
                flag = false;
            if (flag) {
                //判断密码正确性
                flag = checkPsd($("#psd_ipt").val());
                //检查Hub账号长度
                if (flag) {
                    switch ($("#account_ipt").val().length) {
                        case 5:
                            $(location).attr("href", "admin.html");
                            break;
                        case 6:
                            $(location).attr("href", "trainer.html");
                            break;
                        case 8:
                            $(location).attr("href", "member.html");
                            break;
                        default:
                            layer.msg("请再重新检查账号长度！")
                    }
                } else {
                    layer.msg("请再重新检查密码长度!");
                }
            } else {
                layer.msg("哦哦，好像还有没有填写的地方哦，请重新仔细检查")
            }
        } else {
            //2. 短信验证方式
            // 检查“地区选择”，“手机号输入”，“验证码输入”是否都填满了
            let country = $("#country_select").find("option:selected").attr("value"),
                phone = $("#phone_ipt").val(),
                code = $("#code_ipt").val();
            if (country === '0' || phone === undefined || phone === "" || code === undefined || code == "")
                flag = false;

            if (flag) {
                //判断手机号正确性
                flag = checkPhone(phone);
                //检查Hub账号长度
                if (flag) {
                    $(location).attr("href", "member.html");
                } else {
                    layer.msg("请再重新检查手机号长度!");
                }
            } else {
                layer.msg("哦哦，好像还有没有填写的地方哦，请重新仔细检查")
            }
        }

    })

})