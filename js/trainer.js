$(function() {
    // 控制左侧侧边栏的按钮
    var isHidden = false;
    $(".slider").parents(".layui-nav-item").on("click", function() {
        if (isHidden) {
            $(".sidebar_left").stop().animate({ left: '0' }).show();
        } else {
            $(".sidebar_left").stop().animate({ left: '-300px' });
        }
        isHidden = !isHidden;
    });


    // 点击复选框：改变其样式（在选中和未选中之间切换）
    $(".layui-form-checkbox").on("click", function() {
        // 点击切换效果
        if ($(this).attr("data-checked") === "false") {
            $(this).attr("data-checked", "true");
            $(this).addClass("layui-form-checked");
        } else {
            $(this).attr("data-checked", "false");
            $(this).removeClass("layui-form-checked");
        }
    })

})