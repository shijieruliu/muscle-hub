$(function() {
    //加载layui的element模块
    layui.use('element', function() {
        var element = layui.element;

        element.on('tab(demo)', function(data) {});
    });

    // 加载layui的layer模块
    layui.use('layer', function() {
        var layer = layui.layer;
    });

    //课程操作信息提示
    $("#pay_for_it").click(function() {
        $(".course_pay_msg").stop().fadeIn('linear').fadeOut(2500, 'linear');
    })
    $(".select_course").click(function() {
        $(".course_select_msg").stop().fadeIn('linear').fadeOut(2500, 'linear');
    })


    //用于确认点击“即将开始”模块时点击的课程
    var course_index = 0;

    //支付模块的弹出层
    //1. 二维码加载出来后“加载”图标消失
    $(".qrcode").find("img").on("load", function() {
        $(this).siblings("i").hide();
    });
    //2. 点击关闭按钮或页面其他地方或“我再想想”或"确认支付"关闭弹出层
    $("#cancel, #close, #pay_for_it").click(function() {
        $(".pay_sction").hide();
    });
    $(".pay_sction").click(function() {
        $(this).hide();
    });
    $(".wrapper").on("click", function(event) {
        event.stopPropagation();
    });

    // 控制左侧侧边栏的按钮
    var isHidden = false;
    $(".slider").parents(".layui-nav-item").on("click", function() {
        if (isHidden) {
            $(".sidebar_left").stop().animate({ left: '0' }).show();
        } else {
            $(".sidebar_left").stop().animate({ left: '-300px' });
        }
        isHidden = !isHidden;
    });

    // 右侧侧边栏
    $(window).scroll(function() {
        // console.log($("html, body").scrollTop());
        if ($(window).scrollTop() > 400) {
            $(".sidebar_right").fadeIn(1000);
        } else {
            $(".sidebar_right").fadeOut(1000);
        }
    });
    //当点击跳转链接后，回到页面顶部位置
    $("#top_btn").click(function() {
        $('body,html').animate({
                scrollTop: 0
            },
            1000);
        return false;
    });

    // 即将开始模块
    // 1. 点击“支付”按钮,出现弹出层进行支付(此处有些按钮是动态生成的)
    $(".mask").on("click", ".payfor_course", function() {
        //获取此时课程下标
        course_index = $(this).parents("li").index();
        $(".pay_sction").show();
    });
    // 2.点击“确认支付”按钮，将课程添加到“正在进行”模块，移除出“即将开始”模块
    $("#pay_for_it").click(function() {
        //深拷贝：会将事件也拷贝过去
        let node1 = $("#upcoming_course").find("li").eq(course_index).clone(true);
        //显示小红点提示
        $("#badge_Underway").css("display", "inline-block");
        $("#upcoming_course").find("li").eq(course_index).remove();
        //将课程添加到两个模块里去
        $("#underway_course").find("ul").append(node1);
        $("#underway_course").find(".mask").remove();
    })

    // 已完结课程星级评分
    layui.use(['rate'], function() {
        var rate = layui.rate;
        //绑定并自定义评分文本
        rate.render({
            elem: '#star',
            value: 3, //初始值
            text: true, //显示文本
            setText: function(value) { //自定义文本的回调
                var arrs = {
                    '1': '差',
                    '2': '一般',
                    '3': '满意',
                    '4': '非常满意',
                    '5': '无可挑剔！'
                };
                this.span.text(arrs[value] || (value + "星"));
            }
        })
    });


    //课程购物车模块，
    //1. mouseenter和mouseleave，显示或隐藏遮罩层
    $(".m_course_img").mouseenter(function() {
        $(this).find(".mask").stop().slideDown(800);
    })
    $(".m_course_img").mouseleave(function() {
        $(this).find(".mask").stop().slideUp(800);
    })

    //2. 点击“选择课程”按钮,将课程添加到“即将开始”模块和“全部课程模块”，移除出“课程购物车模块”
    $(".select_course").click(function() {
        //深拷贝：会将事件也拷贝过去
        let node1 = $(this).parents(".m_course_wrap").clone(true);
        let node2 = $(this).parents(".m_course_wrap").clone(true);
        //显示显示小红点提示
        $("#badge_All,#badge_UpComing").css("display", "inline-block");
        $(this).parents(".m_course_wrap").remove();
        //将课程添加到两个模块里去
        let node3 = "<button class='payfor_course layui-btn layui-btn-warm layui-btn-radius'>去支付</button>";
        $("#upcoming_course").find("ul").append(node1);
        $("#upcoming_course").find(".mask").empty().append(node3);
        $("#upcoming_course").find(".mask").hide();
        $("#all_course").find("ul").append(node2);
        $("#all_course").find(".mask").remove();
    })

    //3. 点击小红点所在栏，可以消除掉小红点
    $("#li_All").click(function() {
        $("#badge_All").hide();
    })
    $("#li_UpComing").click(function() {
        $("#badge_UpComing").hide();
    })
    $("#li_Underway").click(function() {
        $("#badge_Underway").hide();
    })


    // 热门课程卡片特效
    // 需要增加：遮罩层 + 选择按钮 + 付款
    $(".course_item").on("mouseenter", function() {
        $(this).stop().animate({
            top: "-2px",
        }, 100)
    })

    $(".course_item").on("mouseleave", function() {
        $(this).stop().animate({
            top: "0px",
        }, 100)
    })

    // 教练模块，鼠标移到卡片上的遮罩层效果
    $(".trainer_info").on("mouseenter", function() {
        $(this).find(".mask").stop().fadeIn();
    })

    $(".trainer_info").on("mouseleave", function() {
        $(this).find(".mask").stop().fadeOut();
    })
})