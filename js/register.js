$(function() {

    //检查电话号码正确性
    function checkPhone(phoneNum) {
        let flag = true;
        // 验证长度和每一位
        if (phoneNum.length != 11) {
            flag = false;
        } else {
            if (isNaN(phoneNum)) {
                flag = false;
            }
        }
        return flag;
    }

    //检查密码长度正确性
    function checkPsd(psd) {
        if (psd.length > 16 || psd.length < 6)
            return false;
        return true;
    }

    // 密码输入栏查看状态切换
    var checkAble = false;
    $("#psd_check_type").click(function() {
        if (!checkAble) {
            $(this).find("img").prop("src", "images/open.png");
            $(this).siblings("#psd_ipt").prop("type", "text");
        } else {
            $(this).find("img").prop("src", "images/close.png");
            $(this).siblings("#psd_ipt").prop("type", "password");
        }
        checkAble = !checkAble;
    })

    //验证密码栏输入格式的合法性
    $("#psd_ipt").blur(function() {
        if ($(this).val() !== "") {
            if (!checkPsd($(this).val())) {
                $(this).siblings(".state.correct_state").hide();
                $(this).siblings(".state.wrong_state").show();
            } else {
                $(this).siblings(".state.correct_state").show();
                $(this).siblings(".state.wrong_state").hide();
            }
        } else {
            $(this).siblings(".state").hide();
        }
    })

    //验证手机号输入格式的合法性（这里为了简化，不考虑外国手机号，认为手机号合法长度为11位）
    $("#phone_ipt").blur(function() {
        var phoneNum = $(this).val();
        if (phoneNum !== "") {
            let flag = checkPhone(phoneNum);

            if (flag) {
                $(this).siblings(".state").hide();
                $(this).siblings(".state.correct_state").show();
            } else {
                $(this).siblings(".state").hide();
                $(this).siblings(".state.wrong_state").show();
            }

        } else {
            $(this).siblings(".state").hide();
        }
    })

    // 点击注册按钮时，判断所有表单控件都填完了
    $("#submit_btn").click(function() {
        let flag = true; //指示表单是否满足提交条件：所有表单项填满 + 密码和手机号正确
        //判断表单的所有项是否都填满了
        $("input[type='text'],input[type='password']").each(function(index, domEle) {
            if ($(domEle).val() === undefined || $(domEle).val() === "") {
                layer.msg("哦哦，好像还有没有填写的地方哦，请重新仔细检查");
                flag = false;
                return false;
            }
        });

        if (flag) {
            //判断密码和手机号正确性
            flag = checkPsd($("#psd_ipt").val());
            flag = checkPhone($("#phone_ipt").val());
            if (flag) {
                $(location).attr("href", "member.html");
            } else {
                layer.msg("请再重新检查密码或手机号!");
            }
        }
    })

})