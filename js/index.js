$(function() {
    //注意：导航 依赖 element 模块，否则无法进行功能性操作
    layui.use('element', function() {
        var element = layui.element;
    });
    layui.use('carousel', function() {
        var carousel = layui.carousel;
        carousel.render({
            elem: '#carousel',
            width: '100%',
            height: '700px',
            arrow: 'always',
            anim: 'default',
            autoplay: false
        });
    });

    //“我们的团队”模块stackCard插件初始化
    var stackedCard = new stackedCards({
        selector: '.stacked-cards',
        layout: "slide",
        transformOrigin: "center",
    });
    stackedCard.init();

    var myDate = new Date;
    var week = myDate.getDay();
    $(".course_time").find("li").eq(week).addClass("current_time");
})