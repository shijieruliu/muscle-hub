$(function() {

    //时间正误判断
    function timeCheck(time) {
        if (isNaN(time) || time < 0)
            return false;
        return true;
    }

    function weightCheck(weight) {
        //人类体重范围：5.45kg - 725 kg
        if (isNaN(weight) || weight < 5.45 || weight > 725)
            return false;
        return true;
    }

    //体重输入栏检查正误
    $("#weight_ipt").blur(function() {
        $(this).siblings(".state").hide();
        let weight = $(this).val();
        if (weight != "") {
            if (!weightCheck(weight)) {
                $(this).siblings(".wrong_state").show();
                $("#cal_ipt").val(""); //清空卡路里读取栏
            } else {
                $(this).siblings(".correct_state").show();
            }
        }
    })

    //时间输入栏检查正误
    $("#time_ipt").blur(function() {
        $(this).siblings(".state").hide();
        let time = $(this).val();
        if (time != "") {
            if (!timeCheck(time)) {
                $(this).siblings(".wrong_state").show();
                $("#cal_ipt").val("");
            } else {
                $(this).siblings(".correct_state").show();
            }
        }
    })

    //点击“清空”按钮，隐藏格式正误提示
    $("#reset_btn").click(function() {
        $(".state").hide();
    })

    //查看结果按钮触发卡路里消耗计算事件
    $("#get_result").click(function() {
        let weight = $("#weight_ipt").val();
        let time = $("#time_ipt").val();
        let sport_type = $("#type_ipt option:selected").attr("value");

        //检查用户是否有某一项没有输入
        if (weight === "") {
            $("#cal_ipt").val("");
            alert("体重栏不能为空，请重新检查！");
            return;
        }
        if (sport_type === "") {
            $("#cal_ipt").val("");
            alert("运动类型不能为空，请重新检查！");
            return;
        }
        if (time === "") {
            $("#cal_ipt").val("");
            alert("运动时间栏不能为空，请重新检查！");
            return;
        }

        //检查用户是否有填写格式错误
        if (!weightCheck(weight)) {
            $("#cal_ipt").val("");
            alert("体重输入有误，请重新检查！");
            return;
        }
        if (!timeCheck(time)) {
            $("#cal_ipt").val("");
            alert("运动时间输入有误，请重新检查！");
            return;
        }

        //计算用户消耗的卡路里
        // 几类运动的卡路里消耗系数：爬山 、普拉提（ 常规）、 力量瑜伽 、 
        //      单车训练（ 100 瓦特）、 单车训练（ 150 瓦特）、 划船机（ 100 瓦特）、     
        //      羽毛球、 蛙泳 、 慢速游泳， 自由泳、 慢速狗刨（ 约50码 ）、     
        //      快速狗刨（ 约75码 ）、椭圆机 、 打台球 
        let factor_arr = [0.193183, 0.0551952, 0.0827929, 0.12419, 0.165586, 0.206983, 0.0965929, 0.0965929, 0.248379, 0.165586, 0.193183, 0.275976, 0.137988, 0.0413976];
        let heat_sum = (factor_arr[sport_type] * weight * time).toFixed(4);
        $("#cal_ipt").val("消耗了" + heat_sum + "卡路里");

    })


})