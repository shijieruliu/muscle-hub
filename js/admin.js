$(function() {
    layui.use('element', function() {
        var element = layui.element;

    });

    // 显示系统时间
    var myDate = new Date;
    var year = myDate.getFullYear(); //获取当前年
    var mon = myDate.getMonth() + 1; //获取当前月
    var date = myDate.getDate(); //获取当前日
    var hour = myDate.getHours(); //获取当前小时数(0-23)
    var minute = myDate.getMinutes(); //获取当前分钟数(0-59)
    var second = myDate.getSeconds(); //获取当前秒
    var week = myDate.getDay();
    var week_arr = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
    $("#time").html(year + "年" + mon + "月" + date + "日" + week_arr[week] + "   " + hour + "时" + minute + "分" + second + "秒");

    layui.use(['element', 'layer', 'util', 'laydate', 'form'], function() {
        var element = layui.element,
            layer = layui.layer,
            util = layui.util,
            $ = layui.$,
            form = layui.form,
            laydate = layui.laydate;

        //头部header事件
        util.event('lay-header-event', {
            //左侧菜单事件
            menuLeft: function(othis) {
                layer.msg('展开左侧菜单的操作', {
                    icon: 0
                });
            },
            //右侧菜单事件
            menuRight: function() {
                layer.open({
                    type: 1,
                    content: '<div style="padding: 15px;">没有待办事项</div>',
                    area: ['260px', '100%'],
                    offset: 'rt', //右上角
                    anim: 5,
                    shadeClose: true
                });
            }
        });

        //绑定时间选择器
        laydate.render({
            elem: '#addJoinTime'
        });
        laydate.render({
            elem: '#addTrainerShift'
        })


        //数据表格渲染及其事件
        layui.use(['table'], function() {
            var $ = layui.jquery;
            var table = layui.table;

            // 用户列表相关数据操作start
            // 静态表格渲染
            var userTable = table.init('userTable', {
                title: "用户数据", //导出数据表格时的标题
                cellMinWidth: 80, //全局定义常规单元格的最小宽度
                height: 'full',
                page: true, // 分页开启
                id: 'userTable',
                toolbar: '#headerTool',
                defaultToolbar: [],
                text: {
                    none: '暂无相关数据' //没有数据时的表格内容
                },
            });

            //搜索ID功能实现（此处还没成功！！！）
            table.active = {
                reload: function() {
                    var demoReload = $('#searchID');

                    //执行重载
                    table.reload('userTable', {
                        page: {
                            curr: 1 //从第 1 页开始搜索
                        },
                        where: {
                            key: {
                                id: demoReload.val()
                            }
                        }
                    });
                }
            };
            $('.search .layui-btn').on('click', function() {
                var type = $(this).data('type');
                active[type] ? active[type].call(this) : '';
            });

            //监听行内工具栏：编辑成员属性和删除按钮实现
            table.on('tool(userTable)', function(obj) {
                var data = obj.data;
                if (obj.event === 'edit_name') {
                    //编辑姓名
                    layer.prompt({
                        formType: 2, //表示弹出一个多行文本框
                        title: '请输入新的用户名',
                        value: data.username //获取用户名
                    }, function(value, index) {
                        obj.update({
                            username: value //将文本框内容填入用户名
                        });
                        layer.close(index);
                        layer.msg("修改成功！");
                    });
                } else if (obj.event === 'edit_phone') {
                    //编辑电话
                    layer.prompt({
                        formType: 2, //表示弹出一个多行文本框
                        title: '请输入新的电话号码',
                        value: data.phoneNum //获取用户电话号码
                    }, function(value, index) {
                        obj.update({
                            phoneNum: value //将文本框内容填入用户名
                        });
                        layer.msg("修改成功");
                        layer.close(index);
                    });
                } else if (obj.event === 'del_row') {
                    //删除成员
                    layer.confirm('真的删除该成员么', function(index) {
                        obj.del(); //删除该行
                        layer.close(index); //关闭弹出层
                        layer.msg("删除成功");
                    });
                }
            });

            // 监听表格头部工具栏，实现添加成员功能
            table.on('toolbar(userTable)', function(obj) {
                if (obj.event === 'addMember') {
                    //打开一个表单来接受新成员信息
                    var _index = layer.open({
                        type: 1,
                        title: '添加用户',
                        content: $("#addMemberFrm"),
                        area: ['800px', '450px'],
                        success: function(index) {
                            // 清空表单数据 （每次关闭前）   
                            $("#dataFrm")[0].reset();
                        }
                    });
                    //提交按钮触发事件
                    form.on('submit(doSubmit)', function(data, index) {
                        //先存储表单上的值
                        var userid = $("#addID").val(),
                            username = $('#addUserName').val(),
                            sex = ($("#addSex").val() == '1') ? '男' : '女', //1是男
                            city = $("#addCity").val(),
                            email = $('#addEmail').val(),
                            phone = $('#addPhoneNum').val(),
                            joinTime = $('#addJoinTime').val(),
                            courseNum = $("#addCourseNum").val(),
                            statisfaction = $("#addSta").val();

                        //存储旧数据
                        var oldData = table.cache['userTable'];
                        //新数据覆盖原数据
                        oldData.push({
                                "id": userid,
                                "username": username,
                                "sex": sex,
                                "city": city,
                                "email": email,
                                "phoneNum": phone,
                                "joinTime": joinTime,
                                "courseNum": courseNum,
                                "satisfaction": statisfaction,
                            })
                            //表格重新渲染
                        table.reload('userTable', {
                            url: '',
                            data: oldData
                        });
                        layer.close(_index);
                        layer.msg("成功增加新成员！");
                    });

                }

            });
            //用户列表相关数据操作  end

            //教练列表相关数据操作 start
            var trainerTable = table.init('trainerTable', {
                title: "教练数据", //导出数据表格时的标题
                cellMinWidth: 80, //全局定义常规单元格的最小宽度
                height: 'full',
                page: true, // 分页开启
                id: 'trainerTable',
                toolbar: '#headerTrainerTool',
                defaultToolbar: [],
                text: {
                    none: '暂无相关数据' //没有数据时的表格内容
                },
            });

            // 监听表格头部工具栏，实现添加教练功能
            table.on('toolbar(trainerTable)', function(obj) {
                if (obj.event === 'addTrainer') {
                    var _index = layer.open({
                        type: 1,
                        title: '添加教练',
                        content: $("#addTrainerFrm"),
                        area: ['800px', '450px'],
                        success: function(index) {
                            // 清空表单数据 （每次关闭前）   
                            $("#dataTrainerFrm")[0].reset();
                        }
                    });
                    //表单提交事件
                    form.on('submit(doTrainerSubmit)', function(data, index) {
                        var trainerid = $("#addTrainerID").val(),
                            trainername = $('#addTrainerName').val(),
                            sex = ($("#addTrainerSex").val() == '1') ? '男' : '女', //1是男
                            phoneNum = $("#addTrainerPhone").val(),
                            shift = $('#addTrainerShift').val(),
                            courseNum = $('#addTrainerCourseNum').val(),
                            likeNum = $('#addTrainerLike').val();

                        var oldData = table.cache['trainerTable'];
                        oldData.push({
                            "id": trainerid,
                            "trainerName": trainername,
                            "sex": sex,
                            "phoneNum": phoneNum,
                            "shift": shift,
                            "courseNum": courseNum,
                            "likeNum": likeNum,
                        })
                        table.reload('trainerTable', {
                            url: '',
                            data: oldData
                        });
                        layer.close(_index);
                        layer.msg("成功增加新教练！");
                    });

                }

            });

            //监听行内工具栏：编辑教练属性和删除按钮实现
            table.on('tool(trainerTable)', function(obj) {
                var data = obj.data;
                if (obj.event === 'edit_shift') {
                    layer.prompt({
                        formType: 2, //表示弹出一个多行文本框
                        title: '请输入新的教练轮班信息',
                        value: data.shift //获取教练轮班
                    }, function(value, index) {
                        obj.update({
                            shift: value //将文本框内容填入轮班
                        });
                        layer.close(index);
                        layer.msg("修改成功！");
                    });
                } else if (obj.event === 'edit_phone') {
                    layer.prompt({
                        formType: 2, //表示弹出一个多行文本框
                        title: '请输入新的电话号码',
                        value: data.phoneNum //获取教练电话号码
                    }, function(value, index) {
                        obj.update({
                            phoneNum: value //将文本框内容填入电话号码
                        });
                        layer.msg("修改成功!");
                        layer.close(index);
                    });
                } else if (obj.event === 'del_row') {
                    layer.confirm('真的删除该教练么', function(index) {
                        obj.del(); //删除该行
                        layer.close(index); //关闭弹出层
                        layer.msg("删除成功!");
                    });
                }
            });



        })


    });

});